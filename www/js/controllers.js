// Copyright Juan Garcia @CintaNegra
angular.module('starter.controllers', [])
.controller('TimerController',function($scope, $stateParams, $ionicModal, $timeout,$ionicNavBarDelegate,$rootScope,$ionicPopup,$state){
$ionicNavBarDelegate.showBackButton(false);
$scope.tiempo = $stateParams.tiempo
$scope.selectTimer = function(val) {
    $scope.timeForTimer = val;
    $rootScope.timer = val
    $scope.started = false;
    $scope.paused = false;
    $scope.done = false;
  };
  var mytimeout = null; // the current timeoutID
 // actual timer method, counts down every second, stops on zero
 $scope.onTimeout = function() {
   if ($rootScope.timer === 0) {
     $scope.$broadcast('timer-stopped', 0);
     $timeout.cancel(mytimeout);
     return;
   }
   $rootScope.timer--;
   mytimeout = $timeout($scope.onTimeout, 1000);
 };
 // functions to control the timer
 // starts the timer
 $scope.startTimer = function() {
   mytimeout = $timeout($scope.onTimeout, 1000);
   $scope.started = true;
 };

 // stops and resets the current timer
 $scope.stopTimer = function(closingModal) {
   if (closingModal != true) {
     $scope.$broadcast('timer-stopped', $rootScope.timer);
   }
   var confirmPopup =  $ionicPopup.confirm({
                  title: "Mensaje Tarifa2",
                  template: 'Al cancelar parqueo el sistema verificara su ubicacion. si esta ha cambiado podria generar multa'
                });

                confirmPopup.then(function(res) {
                  if(res) {
                            console.log($rootScope.timer);
                            $rootScope.timer = 0;
                            $rootScope.parqueado = false;
                            $rootScope.noparqueado = true;
                            $scope.started = false;
                            $scope.paused = false;
                            $scope.tiempo = 0;
                            $timeout.cancel(mytimeout);
                            $state.go('cuenta');
                  } else {
                   $ionicpopUp.hide();
                  }
                });


 };
 // pauses the timer
 $scope.pauseTimer = function() {
   $scope.$broadcast('timer-stopped', $rootScope.timer);
   $scope.started = false;
   $scope.paused = true;
   $timeout.cancel(mytimeout);
 };

 // triggered, when the timer stops, you can do something here, maybe show a visual indicator or vibrate the device
 $scope.$on('timer-stopped', function(event, remaining) {
   if (remaining === 0) {
     $scope.done = true;
   }
 });
 // UI
 // When you press a timer button this function is called
$scope.volver = function(){
    $state.go('app.inicio');
}

 // This function helps to display the time in a correct way in the center of the timer
 $scope.humanizeDurationTimer = function(input, units) {
   // units is a string with possible values of y, M, w, d, h, m, s, ms
   if (input == 0) {
     return 0;
   } else {
     var duration = moment().startOf('day').add(input, units);
     var format = "";
     if (duration.hour() > 0) {
       format += "H[h] ";
     }
     if (duration.minute() > 0) {
       format += "m[m] ";
     }
     if (duration.second() > 0) {
       format += "s[s] ";
     }
     return duration.format(format);
   }
 };
 // function for the modal


})
.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

      //controlador global

  $scope.loginData = {};

  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('InicioCtrl', function($scope, DatosUsuario,$rootScope,uiGmapGoogleMapApi,$ionicLoading,$q, uiGmapIsReady,$ionicPopup,$state,$cordovaToast) {

          // selecccion de carro



          $scope.autos  =  $rootScope.gautos;

          // respuesta de seleccion e auto

          $scope.seleccion = {};
          $scope.verAuto = function(){
                console.log($scope.seleccion);
          }



          // controlador de pantalla principal con mapa
         // inicializar mapa con valores iniciales
  $scope.direccionR ="";
  $scope.direccion_actual ="";
  $scope.map = {

                    center : {
                    latitude: "",
                    longitude: ""
                    },
                    zoom : 16,
                    options:{
                      disableDefaultUI: true
                    },
                    control : {}
  };
  $scope.marker = {
      id: 0,
      coords: {
      latitude: "",
      longitude: ""
              },
      options: {
        icon: "img/marcador.png"
                },

                    };
// loading  mientras geolocaliza
$ionicLoading.show({
  templateUrl: "templates/loaders/loader_cuadrados.html",
  animation: 'fade-in',
}).then(function(){
  console.log('mostrado');
});

//metodo para geolocalizar(promise en app.js)
$rootScope.coords().then(function(res){
  $scope.map = {

                    center : {
                    latitude: res.coords.latitude,
                    longitude: res.coords.longitude
                    },
                    zoom : 16,

                    options :{
                        disableDefaultUI: true
                    },
                    control : {}
                    };
  //colocando marcador en posicion encontrada
  $scope.marker = {

      id : 0,
      coords:{
        latitude : res.coords.latitude,
        longitude : res.coords.longitude,
      },
      options : {
              draggable: false,
              icon : "img/marcador.png"
      }

  };
  var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(res.coords.latitude, res.coords.longitude);
      var request = {
        latLng: latlng
      };
      geocoder.geocode(request, function(data, status) {

        if (status == google.maps.GeocoderStatus.OK) {
          if (data[0] != null) {

            $scope.direccionR = data[0].formatted_address;
            $scope.direccion_actual = data[0].formatted_address;

          } else {
            console.log('No hay direccion disponible');
          }
        }
      })

  //ocultando loading
  $ionicLoading.hide().then(function(){
        console.log("ocultada");
  })
})


//// metodo para popup al especificar tiempo de parqueo
$scope.confirmarParqueo = function(tiempo) {

var confirmPopup =  $ionicPopup.confirm({
               title: "Mensaje Tarifa2",
               template: 'Esta Seguro parquearse en este lugar?. TARIFA2 bloqueara la posicion y será debitado en su cuenta el tiempo de parqueo'
             });

             confirmPopup.then(function(res) {
               if(res) {
                 $rootScope.parqueado = true;
                 $rootScope.noparqueado = false;
                 $state.go('app.timer',{tiempo : tiempo})
               } else {
                $ionicpopUp.hide();
               }
             });
};
$scope.parquear = function(minutos){

        $scope.ConfirmarParqueo();

        console.log(minutos);
}





    })
.controller('LoginController',function($scope,$rootScope,$ionicSideMenuDelegate,$cordovaToast,$state){


      $ionicSideMenuDelegate.canDragContent(false);

      $scope.hacerLogin = function(){
          $state.go('app.inicio');
      }

})
.controller('SignupController',function($scope,$rootScope,$ionicSideMenuDelegate,$cordovaImagePicker,$state){
                //mostrar y ocultar formulario

                $scope.data = {
              step: 1,
                          form: {
                           nombre: "",
                           email: "",
                           telefono:""
                          }
              }
              //opciones de la camara

              var options = {
                       maximumImagesCount: 10,
                       width: 800,
                       height: 800,
                       quality: 80
                      };

        //IMAGEN PICKER

        $scope.pickImagen = function(){
            $cordovaImagePicker.getPictures(options)
                .then(function (results) {
                for (var i = 0; i < results.length; i++) {
                console.log('Image URI: ' + results[i]);
                }
                }, function(error) {
                console.log(error);
                });

        }


          $scope.submit = function() {

                $state.go('app.inicio');
        }

        $scope.nextStep = function() {
          $scope.data.step += 1;
        }
        $scope.lastStep = function() {
          $scope.data.step -= 1;
        }
      $ionicSideMenuDelegate.canDragContent(false);

})
.controller('VehiculosController',function($rootScope,$scope, DatosUsuario,$ionicModal,$cordovaToast){

      $scope.vehiculo = {};
          //controlador global

      $scope.loginData = {};

      $ionicModal.fromTemplateUrl('templates/modalNewCarro.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });

      // Triggered in the login modal to close it
      $scope.closeAdd = function() {

        $scope.modal.hide();


      // guardar addAuto



      }


      $scope.guardar = function(){

        $scope.vehiculo.id = $rootScope.gautos.length + 1;
        $scope.vehiculo.saldo = 0;
        $scope.vehiculo.infracciones = 0;
        $scope.vehiculo.tiempo = 0;
        $rootScope.gautos.push($scope.vehiculo);
        $scope.closeAdd();
        $cordovaToast.show('Auto agregado','long','center')
          .then(function(success){
                  console.log(success);
            }),function(error){
                  console.log(error);
            }
      };

      // Open the login modal
      $scope.addAuto = function() {
        $scope.modal.show();
      };

      // Perform the login action when the user submits the login form
      $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
          $scope.closeLogin();
        }, 1000);
      };
})
.controller('RecargasController',function($scope,DatosUsuario,$rootScope){
      $scope.autoSeleccionado = {};
      $scope.showDetail = false;
      $scope.recarga = {};

      $scope.seleccionarAuto = function(auto){
              $scope.autoSeleccionado = auto;
              $scope.showDetail = true;
              console.log(auto);
      }
      $scope.recargar = function(){
                console.log($scope.recarga.monto);
                for (var i = 0; i < $rootScope.gautos.length; i++) {

                      if($rootScope.gautos[i].id == $scope.autoSeleccionado.id  ){

                              $rootScope.gautos[i].saldo = parseInt($rootScope.gautos[i].saldo)  +  parseInt($scope.recarga.monto);
                                          $cordovaToast.show('Saldo recargado','long','center')
                                            .then(function(success){
                                                    console.log(success);
                                              }),function(error){
                                                    console.log(error);
                                              }



                      }
                }
      }

})
.controller('SaldosController',function($scope,DatosUsuario){


})
.controller('TransferenciasController',function($scope,DatosUsuario,$cordovaToast){
    $scope.showDetail = false;
    $scope.seleccionarAuto= function(auto){
          $scope.showDetail = true;
          console.log(auto);
    }
    $scope.transferir = function(){

          $cordovaToast.show('Se ha transferido ','long','center')
              .then(function(success){
                    $scope.showDetail = false;
                });
    }

})
.controller('InfraccionesController',function($scope,DatosUsuario){


})
