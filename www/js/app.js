// Copyright Juan Garcia @CintaNegra
angular.module('starter', ['ionic', 'starter.controllers','starter.services','ionic.cloud','ngCordova','ngStorage','uiGmapgoogle-maps','angularReverseGeocode', 'ion-floating-menu','angular-svg-round-progressbar'])

.config(function($ionicCloudProvider) {
  $ionicCloudProvider.init({
    "core": {
      "app_id": "b6185daf"
    },
    "push": {
      "sender_id": "164492807581",
      "pluginConfig": {
        "ios": {
          "badge": true,
          "sound": true
        },
        "android": {
          "iconColor": "#343434"
        }
      }
    }
  });
})
// configurar el key para google maps

.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
           key: 'AIzaSyAD-u8RjZs7jh31RH7uTp2dyWOGD2KOv2A',
        v: '3.20',
        libraries: 'weather,geometry,visualization'
    });
})


.run(function($ionicPlatform,$rootScope,$ionicPopup,$ionicLoading,$window,$q,$state,$cordovaGeolocation,$cordovaLocalNotification,$ionicPush,DatosUsuario) {
    //esta variable global indica si el usuario no se encuentra parqueado

            $rootScope.gautos = DatosUsuario.get();

            $ionicPush.register().then(function(t) {
          return $ionicPush.saveToken(t);
        }).then(function(t) {
          console.log('Token saved:', t.token);
        });

        $rootScope.$on('cloud:push:notification', function(event, data) {
            var msg = data.message;
            alert(msg.title + ': ' + msg.text);
          });

  $rootScope.parqueado = false;
  $rootScope.noParqueado = true;
  $rootScope.timer = '';
  $rootScope.colorTimer = '#3867ad'

  $rootScope.$watch('timer',function(){
        console.log($rootScope.timer);
      if ($rootScope.timer == 1790){

            console.log("te quedan 5 minutos");
            $rootScope.colorTimer = '#d21414';
            $cordovaLocalNotification.schedule({
                              id: 1,
                              title: '(Prueba de Notificacion)',
                              text: 'te quedan 29 minutos con 50 Segundos (Prueba)',
                              icon:'tarifa2',
                              data: {
                              customProperty: 'custom value'
                              }
                              }).then(function (result) {
                              console.log('te quedan 5 minutos');
                            });



      }



        });





  $rootScope.coords = function() {
        var deferred = $q.defer();


  var posOptions = {timeout: 10000, enableHighAccuracy: true};
  $cordovaGeolocation
  .getCurrentPosition(posOptions)
  .then(function (position) {

    deferred.resolve(position);
    var lat  = position.coords.latitude;
    var long = position.coords.longitude

  }, function(err) {

     $ionicLoading.hide().then(function(){


  confirmPopup.then(function(res) {
   if(res) {
     $window.location.reload(true);
   } else {
     ionic.Platform.exitApp();
   }
 });

});




});



return deferred.promise;
};

  $ionicPlatform.ready(function() {



    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.inicio', {
      url: '/inicio',
      views: {
        'menuContent': {
          templateUrl: 'templates/inicio.html',
          controller: 'InicioCtrl'
        }
      }
    })
    .state('app.timer', {
      url: '/timetr?tiempo',
      views: {
        'menuContent': {
          templateUrl: 'templates/timer.html',
          controller:'TimerController'
        }
      }
    })
    .state('app.vehiculos', {
      url: '/vehiculos',
      views: {
        'menuContent': {
          templateUrl: 'templates/vehiculos.html',
          controller:'VehiculosController'
        }
      }
    })
    .state('app.recargas', {
      url: '/recargas',
      views: {
        'menuContent': {
          templateUrl: 'templates/recargas.html',
          controller:'RecargasController'
        }
      }
    })
    .state('app.transferencias', {
      url: '/transferencias',
      views: {
        'menuContent': {
          templateUrl: 'templates/transferencias.html',
          controller:'TransferenciasController'
        }
      }
    })
    .state('app.infracciones', {
      url: '/infracciones',
      views: {
        'menuContent': {
          templateUrl: 'templates/infracciones.html',
          controller:'InfraccionesController'
        }
      }
    })

.state('login', {
   url: '/login',
   controller: 'LoginController',
   templateUrl: 'templates/tab-signin.html'

 })
 .state('signup', {
    url: '/signup',
    templateUrl: 'templates/tab-signup.html',
    controller: 'SignupController'
  })
  .state('app.saldos', {
    url: '/saldos',
    views: {
      'menuContent': {
        templateUrl: 'templates/saldos.html',
        controller:'SaldosController'
      }
    }
  })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('login');
});
